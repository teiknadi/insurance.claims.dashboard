import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Message } from 'semantic-ui-react';
import { graphql, gql } from 'react-apollo'
import Claim from './component/Claim';
import Preloader from '../../../../components/Preloader';

class InsuranceClaims extends Component {
  render() {
    if (this.props.data.loading) {
      return <Preloader page={true} />
    }
    if (!this.props.data.allClaims) {
      return <Message negative>There are no results matching given query.</Message>
    }

    return (
      <Table compact celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Policy ID</Table.HeaderCell>
            <Table.HeaderCell>Claim Type</Table.HeaderCell>
            <Table.HeaderCell>Claim Amount</Table.HeaderCell>
            <Table.HeaderCell>Date Occurred</Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell width="1" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.props.data.allClaims.map((claim) =>
            <Claim key={claim.id} {...claim} />
          )}
        </Table.Body>
      </Table>
    );
  }
}

InsuranceClaims.propTypes = {
  data: PropTypes.object,
};

const InsuranceClaimsQuery = gql`query InsuranceClaimsQuery {
  allClaims(orderBy: createdAt_DESC) {
    id
    name
    emailAddress
    policyId
    claimType
    claimAmount
    dateOccurred
    claimStatus
  }
}`

export default graphql(InsuranceClaimsQuery, {options: {fetchPolicy: 'network-only'}})(InsuranceClaims)

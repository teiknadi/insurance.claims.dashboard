import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Label, Button, Icon } from 'semantic-ui-react'
import { graphql, gql } from 'react-apollo';
import CONFIG from '../../../../../../config';
import { isPending, claimStatuses, getClaimTypeName } from '../../../../../../services/claimHelpers';

class Claim extends Component {
  state = {
    status: this.props.claimStatus,
  };

  handleApprove = () => {
    this.props.updateClaim({variables: { id: this.props.id, claimStatus: claimStatuses.approved }});
    this.setState({ status: claimStatuses.approved });
  };

  handleReject = () => {
    this.props.updateClaim({variables: { id: this.props.id, claimStatus: claimStatuses.rejected }});
    this.setState({ status: claimStatuses.rejected });
  };

  render() {
    const { name, emailAddress, policyId, claimType, claimAmount, dateOccurred } = this.props;
    const { status } = this.state;
    const isStatusPending = isPending(status);

    const newLabel = isStatusPending ? <Label ribbon size="mini" color="blue">New</Label> : '';

    let statusCell = '';
    switch(status) {
      case claimStatuses.approved:
        statusCell = <Table.Cell textAlign="center" positive><Icon name='checkmark' />{status}</Table.Cell>
        break;
      case claimStatuses.rejected:
        statusCell = <Table.Cell textAlign="center" negative><Icon name='attention' />{status}</Table.Cell>
        break;
      default:
        statusCell = <Table.Cell textAlign="center">{status}</Table.Cell>
    }

    return (
      <Table.Row>
        <Table.Cell>{newLabel} {name}</Table.Cell>
        <Table.Cell>{emailAddress}</Table.Cell>
        <Table.Cell>{policyId}</Table.Cell>
        <Table.Cell>{getClaimTypeName(claimType)}</Table.Cell>
        <Table.Cell textAlign="right">{claimAmount} {CONFIG.currency.symbol}</Table.Cell>
        <Table.Cell textAlign="right">{dateOccurred}</Table.Cell>
        {statusCell}
        <Table.Cell width="1">
          <Button.Group>
            <Button icon='checkmark' size="mini" color="green" disabled={!isStatusPending} onClick={this.handleApprove} />
            <Button icon='attention' size="mini" color="red" disabled={!isStatusPending} onClick={this.handleReject} />
          </Button.Group>
        </Table.Cell>
      </Table.Row>
    )
  }
}

Claim.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  emailAddress: PropTypes.string.isRequired,
  policyId: PropTypes.string.isRequired,
  claimType: PropTypes.string.isRequired,
  claimAmount: PropTypes.string.isRequired,
  dateOccurred: PropTypes.string.isRequired,
  claimStatus: PropTypes.string.isRequired,
  updateClaim: PropTypes.func.isRequired,
};

const updateClaim = gql`
  mutation updateClaim($id: ID!, $claimStatus: String!) {
    updateClaim(id: $id, claimStatus: $claimStatus) {
      id
    }
  }
`

export default graphql(updateClaim, {name: 'updateClaim'})(Claim);

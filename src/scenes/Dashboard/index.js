import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types';
import InsuranceClaims from './components/InsuranceClaims'

class Dashboard extends Component {
  render() {
    const { isAuthenticated } = this.props.auth;

    // redirect if no user is logged in
    if (!isAuthenticated()) {
      return <Redirect to={{
        pathname: '/',
        state: {
          message: {
            type: 'warning',
            text: "Only logged in users can see the dashboard"
          }
        }
      }} />
    }

    return <InsuranceClaims />
  }
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
};

export default Dashboard;

import React  from 'react';
import { Redirect } from 'react-router-dom'
import Preloader from '../../components/Preloader';

const AuthCallback = ({ auth, location }) => {
  const { isAuthenticated } = auth;

  // redirect if user is logged in
  if (isAuthenticated()) {
    return <Redirect to={{
      pathname: '/',
      state: {
        message: {
          type: 'error',
          text: "Only logged out users can be authenticated"
        }
      }
    }} />
  }

  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }

  return <Preloader page={true}/>;
};

export default AuthCallback;

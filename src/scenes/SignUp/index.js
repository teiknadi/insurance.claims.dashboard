import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Redirect } from 'react-router-dom';
import { graphql, gql } from 'react-apollo';
import { Container, Form, Button, Header } from 'semantic-ui-react';
import FormInputField from '../../components/FormInputField';
import Preloader from '../../components/Preloader';

class SignUp extends React.Component {
  state = {
    emailAddress: '',
    name: ''
  };

  handleInputChange = (event, { name, value }) => {
    this.setState({
      [name]: value
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const variables = {
      idToken: window.localStorage.getItem('auth0IdToken'),
      name: this.state.name,
      emailAddress: this.state.emailAddress
    };

    this.props.createUser({ variables })
      .then((response) => {
        this.props.history.replace('/')
      }).catch((e) => {
        console.error(e);
        this.props.history.replace('/')
      })
  };

  render () {
    if (this.props.data.loading) {
      return <Preloader page={true} />
    }

    // redirect if user is logged in or did not finish Auth0 Lock dialog
    if (this.props.data.user || window.localStorage.getItem('auth0IdToken') === null) {
      return (
        <Redirect to={{  pathname: '/' }}/>
      )
    }

    return (
      <Container>
        <Header>Sign in using Auth0</Header>
        <Form>
          <FormInputField
            label="Email"
            name="emailAddress"
            placeholder="Enter your email address"
            value={this.state.emailAddress}
            onChange={this.handleInputChange}
          />
          <FormInputField
            label="Name"
            name="name"
            placeholder="Enter your full name"
            value={this.state.name}
            onChange={this.handleInputChange}
          />

          <Button primary type='submit' disabled={this.state.name ? false : true} onClick={this.handleSubmit}>Sign up</Button>
        </Form>
      </Container>
    )
  }
}

SignUp.propTypes = {
  createUser: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const createUser = gql`
  mutation createUser($idToken: String!, $name: String!, $emailAddress: String!){
    createUser(authProvider: { auth0: { idToken : $idToken } }, name: $name, emailAddress: $emailAddress) {
      id
    }
  }
`

const userQuery = gql`
  query {
    user {
      id
    }
  }
`

export default graphql(createUser, {name: 'createUser'})(
  graphql(userQuery, {options: {fetchPolicy: 'network-only'}})(withRouter(SignUp))
)

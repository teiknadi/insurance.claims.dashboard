import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { graphql, gql } from 'react-apollo';
import { Button, Form, Message } from 'semantic-ui-react';
import moment from 'moment';
import revalidator from 'revalidator';
import FormInputField from '../../../../components/FormInputField';
import FormSelectField from '../../../../components/FormSelectField';
import FormDateField from '../../../../components/FormDateField';
import { claimTypes, validationRules } from '../../../../services/claimHelpers';

class AddInsuranceClaim extends Component {
  state = {
    name: '',
    emailAddress: '',
    policyId: '',
    claimType: 'LostBaggage',
    claimAmount: '',
    dateOccurred: moment(),
    errorMessage: '',
    message: ''
  };

  handleInputChange = (event, { name, value }) => {
    this.setState({
      [name]: value
    });
  };

  handleDateChange = (date) => {
    this.setState({
      dateOccurred: date
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    var formData = { ...this.state };
    formData.dateOccurred = formData.dateOccurred.format("YYYY-MM-DD");

    var formValidation = revalidator.validate(formData, {properties: validationRules});

    if (formValidation.valid) {
      const { name, emailAddress, policyId, claimType, claimAmount, dateOccurred } = formData;

      this.props.createClaim({variables: { name, emailAddress, policyId, claimType, claimAmount, dateOccurred }})
        .then(() => this.showTempMessage('Your claim has been submitted. Please give us a moment for response.'));

      this.setState({
        name: '',
        emailAddress: '',
        policyId: '',
        claimType: 'LostBaggage',
        claimAmount: '',
        dateOccurred: moment(),
        errorMessage: ''
      })
    } else {
      this.setState({
        errorMessage: formValidation.errors.map((error) => error.message)
      })
    }
  };

  showTempMessage = (msg) => {
    this.setState({message: msg});
    setTimeout(() => this.setState({message: ''}), 5000)
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>

        {this.state.errorMessage && <Message negative list={this.state.errorMessage}/>}
        {this.state.message && <Message positive>{this.state.message}</Message>}

        <FormInputField
          label="Name"
          name="name"
          placeholder="Enter your full name"
          value={this.state.name}
          onChange={this.handleInputChange}
        />
        <FormInputField
          label="Email"
          name="emailAddress"
          placeholder="Enter your email address"
          value={this.state.emailAddress}
          onChange={this.handleInputChange}
        />
        <FormInputField
          label="Policy ID"
          name="policyId"
          placeholder="Enter ID of your policy"
          value={this.state.policyId}
          onChange={this.handleInputChange}
        />
        <FormSelectField
          label="Claim Type"
          name="claimType"
          placeholder="Select claim type"
          options={claimTypes}
          value={this.state.claimType}
          onChange={this.handleInputChange}
        />
        <FormInputField
          label="Claim Amount"
          name="claimAmount"
          placeholder="Enter claim amount"
          value={this.state.claimAmount}
          onChange={this.handleInputChange}
        />
        <FormDateField
          label="Date Occurred"
          name="dateOccurred"
          selected={this.state.dateOccurred}
          onChange={this.handleDateChange}
        />

        <Button primary type='submit'>Add insurance claim</Button>
      </Form>
    )
  }
}

AddInsuranceClaim.propTypes = {
  createClaim: PropTypes.func,
};

const createClaim = gql`
  mutation createClaim($name: String!, $emailAddress: String!, $policyId: String!, $claimType: String!, $claimAmount: String!, $dateOccurred: String!) {
    createClaim(name: $name, emailAddress: $emailAddress, policyId: $policyId, claimType: $claimType, claimAmount: $claimAmount, dateOccurred: $dateOccurred) {
      id
    }
  }
`

export default graphql(createClaim, {name: 'createClaim'})(withRouter(AddInsuranceClaim))

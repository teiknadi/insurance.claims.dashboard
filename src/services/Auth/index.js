import auth0Lock from 'auth0-lock';
import history from '../../history';
import CONFIG from '../../config';

class Auth {
  lock = new auth0Lock(CONFIG.auth0.clientId, CONFIG.auth0.domain, {
    auth: {
      redirectUrl: CONFIG.auth0.callbackUrl,
      responseType: 'token',
      sso: true
    }
  });

  isAuthenticated = () => {
    // Check whether the current time is past the access token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'));

    return new Date().getTime() < expiresAt;
  };

  login = () => {
    this.lock.show();
  };

  handleAuthentication = () => {
    let that = this;

    this.lock.on('authenticated', function(authResult) {
      that.setSession(authResult);
      history.replace('/', {
        message: {
          type: 'success',
          text: "Congratulations. You are logged in now. You may now see the claims dashboard."
        }
      });
      window.location.reload();
    });
    this.lock.on('authorization_error', function(err) {
      history.replace('/', {
        message: {
          type: 'error',
          text: "Something went wrong during authentication. Please try again later."
        }
      });
      console.error(err);
    });
  };

  logout = () => {
    this.clearSession();

    // navigate to the home route
    history.replace('/', {
      message: {
        type: 'success',
        text: "You are logged out now."
      }
    });
    window.location.reload();
  };

  setSession(authResult) {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify((authResult.idTokenPayload.exp * 1000) + new Date().getTime());

    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  clearSession() {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
  }
}

export default Auth;

import { ApolloClient, createNetworkInterface } from 'react-apollo';
import CONFIG from '../config';

export const getApolloClient = () => {
  const networkInterface = createNetworkInterface({ uri: CONFIG.graphcool.simpleAPIEndpoint });

  // use the auth0IdToken in localStorage for authorized requests
  networkInterface.use([{
    applyMiddleware (req, next) {
      if (!req.options.headers) {
        req.options.headers = {}
      }

      // get the authentication token from local storage if it exists
      if (localStorage.getItem('auth0IdToken')) {
        req.options.headers.authorization = `Bearer ${localStorage.getItem('auth0IdToken')}`
      }
      next()
    },
  }]);

  return new ApolloClient({ networkInterface });
};

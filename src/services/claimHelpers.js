export const claimStatuses = {
  pending: 'Pending',
  approved: 'Approved',
  rejected: 'Rejected',
};

export const claimTypes = [
  { key: 1, text: 'Lost Baggage', value: 'LostBaggage' },
  { key: 2, text: 'Theft', value: 'Theft' },
  { key: 3, text: 'Missed Flight', value: 'MissedFlight' },
  { key: 4, text: 'Illness', value: 'Illness' },
  { key: 5, text: 'Accident', value: 'Accident' },
];

export const validationRules = {
  name: {
    message: 'Your full name is required',
    type: 'string',
    allowEmpty: false,
    required: true
  },
  emailAddress: {
    message: 'Your proper e-mail address is required, to send you our answer',
    type: 'string',
    format: 'email',
    allowEmpty: false,
    required: true
  },
  policyId: {
    message: 'Proper policy id is required',
    type: 'string',
    allowEmpty: false,
    required: true
  },
  claimType: {
    message: 'Proper claim type is required',
    type: 'string',
    enum: claimTypes.map((option) => option.value),
    allowEmpty: false,
    required: true
  },
  claimAmount: {
    message: 'Proper claim amount is required',
    type: 'string',
    allowEmpty: false,
    required: true
  },
  dateOccurred: {
    message: 'Proper date occurred is required',
    type: 'string',
    format: 'date',
    allowEmpty: false,
    required: true
  },
};

export const isPending = (status) => {
  return status === claimStatuses.pending;
};

export const isApproved = (status) => {
  return status === claimStatuses.approved;
};

export const isRejected = (status) => {
  return status === claimStatuses.rejected;
};

export const getClaimTypeName = (value) => {
  return claimTypes.filter((type) => type.value === value)[0].text;
};

import React from 'react';
import PropTypes from 'prop-types';
import { Dimmer, Loader } from 'semantic-ui-react';

const Preloader = ({ page }) => (
  <Dimmer active inverted page={page}>
    <Loader size='massive'>Loading</Loader>
  </Dimmer>
);

Preloader.propTypes = {
  page: PropTypes.bool,
};

export default Preloader;
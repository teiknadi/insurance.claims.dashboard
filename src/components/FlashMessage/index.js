import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Message, Icon } from 'semantic-ui-react';
import history from '../../history';

class FlashMessage extends Component {
  state = {
    header: '',
    text: '',
    type: ''
  };

  componentDidMount() {
    const { header, text, type } = this.props;

    if (text && type) {
      this.setState({
        header: header,
        text: text,
        type: type
      });

      setTimeout(() => {
        this.setState({
          header: '',
          text: '',
          type: ''
        });

        // reset state in history
        history.replace('/', null);
      }, 5000)
    }
  }

  render() {
    const { header, text, type } = this.state;

    if (!text) {
      return null;
    }

    const messageHeader = header && <Message.Header>{header}</Message.Header>;
    const messageContent = <Message.Content>{text}</Message.Content>;

    switch (type) {
      case 'success':
        return (
          <Message icon success={true}>
            <Icon name='check circle' />
            {messageHeader}
            {messageContent}
          </Message>
        );
      case 'warning':
        return (
          <Message icon warning={true}>
            <Icon name='warning circle' />
            {messageHeader}
            {messageContent}
          </Message>
        );
      case 'error':
        return (
          <Message icon error={true}>
            <Icon name='warning sign' />
            {messageHeader}
            {messageContent}
          </Message>
        );
      default:
        return (
          <Message icon info={true}>
            <Icon name='info circle' />
            {messageHeader}
            {messageContent}
          </Message>
        );
    }
  };
};

FlashMessage.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
};

export default FlashMessage;

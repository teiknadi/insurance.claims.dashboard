import React from 'react';
import { shallow } from "enzyme";
import Navigation from "./index";

describe("Navigation", () => {
  let shallowedNavigation;
  let props = {};

  const navigation = () => {
    if (!shallowedNavigation) {
      shallowedNavigation = shallow(
        <Navigation {...props} />
      );
    }
    return shallowedNavigation;
  };

  beforeEach(() => {
    shallowedNavigation = undefined;
    props = {
      auth: {
        isAuthenticated: jest.fn().mockReturnValue(false)
      }
    };
  });

  describe.only('for not signed in user', () => {

    test('dashboard button should be hidden', () => {
      const actual = navigation().find({ name: 'dashboard'}).length;

      expect(actual).toBe(0);
    });

    test('sign in button should be visible', () => {
      const actual = navigation().find({ name: 'sign-in'}).length;

      expect(actual).toBe(1);
    });

  });
});

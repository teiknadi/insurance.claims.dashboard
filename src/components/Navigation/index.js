import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Container, Menu } from 'semantic-ui-react'
import FlashMessage from '../FlashMessage'

class Navigation extends Component {
  state = {
    activeItem: '/',
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      activeItem: nextProps.location.pathname
    })
  }
  componentDidMount() {
    this.setState({
      activeItem: this.props.location.pathname
    })
  }

  login = () => {
    this.props.auth.login();
  };

  logout = () => {
    this.props.auth.logout();
  };

  render() {
    const { activeItem } = this.state;
    const { state } = this.props.location;
    const { isAuthenticated } = this.props.auth;

    const dashboardItem = (
      <Menu.Item name='dashboard' active={activeItem === '/dashboard'}>
        <Link to="dashboard">Dashboard</Link>
      </Menu.Item>
    );
    const logInItem = (
      <Menu.Item name='sign-in' onClick={this.login}>
        Sing-in
      </Menu.Item>
    );
    const logOutItem = (
      <Menu.Item name='sign-out' onClick={this.logout}>
        Sign-out
      </Menu.Item>
    );

    const flashMessage = state && state.message && <FlashMessage {...state.message} />;

    return (
      <Container>
        <Menu pointing stackable>
          <Menu.Item>
            <Link to="/">
              <img src='/android-icon-48x48.png' alt="Insurance Claims Dashboard" />
            </Link>
          </Menu.Item>

          <Menu.Item name='add-insurance-claim' active={activeItem === '/'}>
            <Link to="/">Add Insurance Claim</Link>
          </Menu.Item>

          { isAuthenticated() ? dashboardItem : ''}

          <Menu.Menu position='right'>
            { isAuthenticated() ? logOutItem : logInItem }
          </Menu.Menu>
        </Menu>
        { flashMessage }
        <br />
      </Container>
    )
  }
}

Navigation.propTypes = {
  auth: PropTypes.object.isRequired,
};

export default Navigation;

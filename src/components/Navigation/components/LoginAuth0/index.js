import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Auth0Lock from 'auth0-lock';
import { withRouter } from 'react-router-dom';

class LoginAuth0 extends Component {

  constructor (props) {
    super(props);

    this.lock = new Auth0Lock(props.clientId, props.domain);
  }

  componentDidMount() {
    this.lock.on('authenticated', (authResult) => {
      window.localStorage.setItem('auth0IdToken', authResult.idToken);

      this.props.history.push(`/signup`)
    })
  }

  showLogin = () => {
    this.lock.show()
  };

  render() {
    return <span onClick={this.showLogin}>Sign-in</span>
  }
}

LoginAuth0.propTypes = {
  clientId: PropTypes.string.isRequired,
  domain: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(LoginAuth0)

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select } from 'semantic-ui-react';

const FormSelectField = ({ label, name, placeholder, options, value, onChange }) => (
  <Form.Field>
    <label>{label}</label>
    <Select
      name={name}
      placeholder={placeholder}
      options={options}
      value={value}
      onChange={onChange}
    />
  </Form.Field>
);

FormSelectField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FormSelectField;

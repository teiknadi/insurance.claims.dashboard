import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const FormDateField = ({ label, name, selected, onChange }) => (
  <Form.Field>
    <label>{label}</label>
    <DatePicker
      name={name}
      selected={selected}
      onChange={onChange}
    />
  </Form.Field>
);

FormDateField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  selected: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FormDateField;

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'semantic-ui-react';

const FormInputField = ({ label, name, placeholder, value, onChange }) => (
  <Form.Field>
    <label>{label}</label>
    <Input
      name={name}
      label={{ icon: 'asterisk' }}
      labelPosition='right corner'
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  </Form.Field>
);

FormInputField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FormInputField;

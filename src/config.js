export default {
  graphcool: {
    simpleAPIEndpoint: 'https://api.graph.cool/simple/v1/cj511qkymatn20196hwoypjtc',
  },
  auth0: {
    domain: 'teiknadi.eu.auth0.com',
    clientId: '6ELIkESE6LBixDU1aDegcP5SeKbU0UFL',
    callbackUrl: 'http://localhost:3000/auth-callback'
  },
  currency: {
    name: 'euro',
    symbol: '€'
  }
};

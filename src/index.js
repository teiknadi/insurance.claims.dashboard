import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { makeMainRoutes } from './routes';
import { getApolloClient } from './services/apolloClient';
import registerServiceWorker from './services/registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';

const client = getApolloClient();
const routes = makeMainRoutes();

ReactDOM.render(
  <ApolloProvider client={client}>{routes}</ApolloProvider>,
  document.getElementById('root')
);

registerServiceWorker();

import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import Auth from './services/Auth';
import Navigation from './components/Navigation';
import Home from './scenes/Home';
import Dashboard from './scenes/Dashboard';
import AuthCallback from './scenes/AuthCallback';
import history from './history';

const auth = new Auth();

export const makeMainRoutes = () => {
  return (
    <BrowserRouter history={history} component={Navigation}>
      <Container>
        <br />
        <Route path='/' render={(props) => <Navigation auth={auth} {...props} />} />
        <Route path='/' exact component={Home} />
        <Route path='/dashboard' render={(props) => <Dashboard auth={auth} {...props} />} />
        <Route path="/auth-callback" render={(props) => <AuthCallback auth={auth} {...props} />} />
        <br />
      </Container>
    </BrowserRouter>
  );
};

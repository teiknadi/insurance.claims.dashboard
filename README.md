# Insurance Claims Dashboard

## Development

1. Clone the repo
2. Run `yarn install` inside the main directory
3. Run `yarn start` to open the app

### Available scripts

- `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

- `yarn test`

Launches the test runner in the interactive watch mode.

- `yarn test -- --coverage`

Launches the test coverage and display results in a handy table.

- `yarn run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
